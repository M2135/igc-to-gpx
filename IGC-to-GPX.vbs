'###################################################################
'IGC-to-GPX-.vbs Version 1.0
'Converts igc files into gpx Dateien
'
'Place the script in the directory which contains the igc file(s) and execute.
'###################################################################


Const ForReading = 1
Set fso = CreateObject("Scripting.FileSystemObject")
Set objFolder = fso.GetFolder(fso.GetAbsolutePathName("."))
Set colFiles = objFolder.Files

strConvertedFiles = ""
For Each oFile in colFiles
    If Right(oFile.Name, 4) = ".igc" Then strConvertedFiles = strConvertedFiles & vbcrlf & ConvertFile(oFile.Name)
Next

wscript.echo "Konvertierte Datei(en): " & strConvertedFiles


Private Function ConvertFile(strFileName)

	Set objFile = fso.OpenTextFile (strFileName, ForReading)

	For n = 1 to 8
		strCurLine = objFile.Readline
		If Left(strCurLine, 5) = "HFDTE" Then strDate = cStr(cInt(Right(strCurLine,2))+2000) & "-" & Mid(strCurLine, 8, 2) & "-" & Mid(strCurLine, 6, 2)
	Next

	xmlTrackPoints = ""
	Do Until objFile.AtEndOfStream

		strCurLine = objFile.Readline

		If Left(strCurLine, 1) = "B" and IsNumeric(Mid(strCurLine, 2, 6)) Then
			xmlTrackPoints = xmlTrackPoints & vbcrlf & ConstructTrackPoint(strCurLine, strDate)
		End If
	Loop

	objFile.Close
	Set objFile = Nothing

	strDescription = "Beschreibung"

	strHead = 	"<?xml version=""1.0"" encoding=""utf-8"" standalone=""yes""?>" & vbcrlf &_
				"<gpx version=""1.1"" creator=""m2135"" " &_
				"xmlns=""http://www.topografix.com/GPX/1/1"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" " &_
				"xsi:schemaLocation=""http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd"">" & vbcrlf &_
				"<trk>" & vbcrlf &_
	  			"<name>" & strFileName & "</name>" & vbcrlf &_
	  			"<desc>" & strDescription & "</desc>" & vbcrlf &_
	 			"<trkseg>"

	strFoot =  "</trkseg>" & vbcrlf &_
				"</trk>" & vbcrlf &_
				"</gpx>"


	strTargetFile = Replace(strFileName, ".igc", ".gpx")

	Set objFile = fso.CreateTextFile(strTargetFile, True)
	objFile.WriteLine(strHead & vbcrlf & xmlTrackPoints & vbcrlf & strFoot)
	objFile.Close

	ConvertFile = strTargetFile

End Function

Private Function ConstructTrackPoint(strData, strDay)

	strLat = Replace(cStr(cDbl(Mid(strData, 8, 2)) + Round(cDbl(Mid(strData, 10, 5))/60000,7)),",", ".")
	strLon = Replace(cStr(cDbl(Mid(strData, 16, 3)) + Round(cDbl(Mid(strData, 19, 5))/60000,7)),",", ".")
	strTrkpt = "<trkpt lat=""" & strLat & """ lon=""" & strLon & """>"
	strDateTime = "<time>" & strDay & "T" & Mid(strData, 2, 2) & ":" & Mid(strData, 4, 2) & ":" & Mid(strData, 6, 2) & "Z" & "</time>"
	strElevation = "<ele>" & Replace(FormatNumber(cStr(Round(cdbl(Right(strData,5)),1)), 1),",", ".") & "</ele>"

	ConstructTrackPoint = strTrkpt & vbcrlf &_
						vbtab & strElevation & vbcrlf &_
						vbtab & strDateTime & vbcrlf &_
						"</trkpt>"

End Function